<?php defined('BASEPATH') OR exit('No direct script access allowed');
use Aws\TranscribeService\TranscribeServiceClient;

class AWS_Transcribe {

	public function __construct($params=null)
	{

		$this->CI =& get_instance();
		$this->CI->load->config('aws');
		$this->CI->config->item('region');

		$this->region = $this->CI->config->item('region');
		$this->bucket = $this->CI->config->item('bucket');
		$this->version = $this->CI->config->item('version');
		$this->access_key = $this->CI->config->item('access_key');
		$this->access_key_secret = $this->CI->config->item('access_key_secret');

		$this->credentials = new Aws\Credentials\Credentials( $this->access_key, $this->access_key_secret);

		$this->config = array
		(
			'version' => $this->version, 
			'region'  => $this->region, 
			'credentials' => $this->credentials,
		);

		$this->clientTranscribe = new TranscribeServiceClient($this->config);

	}

        /**
		*@param language = en-US|es-US|en-AU|fr-CA|en-GB|de-DE|pt-BR|fr-FR|it-IT|ko-KR|es-ES|en-IN|hi-IN|ar-SA', // REQUIRED
		*@param $file é url do arquivo em um bucket no s3
		*@param mediaFormat é o formato do arquivo : Validos = p3|mp4|wav|flac
		*@param $nameJob é nome da trancriação. Este será utilizado para buscar o status da transcrição.
        */
		public function create($file , $nameJob , $language='pt-BR'){

			$mediaFormat = pathinfo($file)['extension'];

			$result = $this->clientTranscribe->startTranscriptionJob([
				'LanguageCode' =>  $language,
				'Media' => [ 
					'MediaFileUri' => $file,
				],
				'MediaFormat' => $mediaFormat,
				'OutputBucketName' => $this->bucket,
				'Settings' => [
					'ChannelIdentification' => false,
					'ShowSpeakerLabels' => false ,
				],
				'TranscriptionJobName' => $nameJob, 
			]);

			return $result->toArray();

		}

		public function get($nameJob){
			$result = $this->clientTranscribe->getTranscriptionJob([
				'TranscriptionJobName' => $nameJob, 
			]);

			return $result->toArray();
		}

		public function delete($nameJob){
			$result = $client->deleteTranscriptionJob([
			    'TranscriptionJobName' => $nameJob, 
			]);

			return $return->toArray();
		}

		public function all($JobNameContains=null,$status='IN_PROGRESS', $number=50){

			if (!!$JobNameContains) {
				$options['JobNameContains']  = $JobNameContains;
			}

			$options['Status'] = $status;
			$options['MaxResults'] = $number;

			$result = $this->clientTranscribe->listTranscriptionJobs($options);

			return $result->toArray()['TranscriptionJobSummaries'];

		}


	}